#include "escola.h"

Escola::Escola(){

}

int Escola::getId(){
	return id;
}

int Escola::getPreferencia(){
	return preferencia;
}

int Escola::getVagas(){
	return vagas;
}

void Escola::setId(int id){
	this->id=id;
}

void Escola::setId(string id){
	id=id.substr(1,id.size());
	this->id=stoi(id);
}

void Escola::setPreferencia(int preferencia){
	this->preferencia=preferencia;
}

void Escola::setPreferencia(string preferencia){
	preferencia=preferencia.substr(1,preferencia.size());
	this->preferencia=stoi(preferencia);
}

void Escola::setVagas(int vagas){
	this->vagas=vagas;
}

void Escola::setVagas(string vagas){
	vagas=vagas.substr(1,vagas.size());
	this->vagas=stoi(vagas);
}

ostream& operator<< (ostream& os, Escola& es){
	os<<"EscolaID: "<<es.id<<", Preferencia: "<<es.preferencia<<", Vagas: "<<es.vagas<<endl;
}