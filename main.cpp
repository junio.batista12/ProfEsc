#include "grafo.h"
#include "professor.h"
#include "vector"

using namespace std;

int main(int argc, char const *argv[]){
	Grafo grafo;
	switch(argc){
		case 1:
			grafo.fillGrafo();
			break;
		default:
			grafo.fillGrafo(argv[1]);
			break;
	}
	return 0;
}