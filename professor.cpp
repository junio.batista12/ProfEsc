#include "professor.h"

Professor::Professor(){

}

int Professor::getId(){
	return id;
}

int Professor::getHabilitacoes(){
	return habilitacoes;
}

vector<int> Professor::getPreferencias(){
	return preferencias;
}

void Professor::setId(int id){
	this->id=id;
}

void Professor::setHabilitacoes(int habilitacoes){
	this->habilitacoes=habilitacoes;
}

void Professor::addPreferencia(int preferencia){
	preferencias.push_back(preferencia);
}

ostream& operator<< (ostream& os, const Professor& pf){
	os<<"ProfID: "<<pf.id<<", Habilitacoes: "<<pf.habilitacoes<<", Preferencias: ";
	for(int i=0;i<pf.preferencias.size();i++){
		os<<pf.preferencias[i];
		if(i!=pf.preferencias.size()-1)
			os<<", ";
	}
	os<<endl;
	return os;
}