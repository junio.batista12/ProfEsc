#ifndef ESCOLA_H
#define ESCOLA_H

#include "string"
#include "iostream"

using namespace std;

class Escola{
private:
	int id, preferencia, vagas;
public:
	Escola();
	int getId();
	int getPreferencia();
	int getVagas();
	void setId(int);
	void setId(string);
	void setPreferencia(int);
	void setPreferencia(string);
	void setVagas(int);
	void setVagas(string);
	friend ostream& operator<<(ostream&, Escola&);
};

#endif