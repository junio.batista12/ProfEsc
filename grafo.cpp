#include "grafo.h"

Grafo::Grafo(){
}

vector<Professor> Grafo::getProfessores(){
	return professores;
}

vector<Escola> Grafo::getEscolas(){
	return escolas;
}

void Grafo::fillGrafo(){
	string input="(";
	Professor ptmp;
	Escola etmp;
	while(input[0]=='('){
		getline(cin, input);
		switch(input[1]){
			case 'P':
				ptmp=splitProfessor(input);
				addProfessor(ptmp);
				break;
			case 'E':
				etmp=splitEscola(input);
				addEscola(etmp);
				break;
			default:
				break;
		}
	}
}

void Grafo::fillGrafo(string fileName){
	ifstream fin;
	string input;
	Escola etmp;
	Professor ptmp;
	fin.open(fileName);
	if(fin.is_open()){
		while(getline(fin,input)){
			switch(input[1]){
				case 'P':
					ptmp=splitProfessor(input);
					addProfessor(ptmp);
					break;
				case 'E':
					etmp=splitEscola(input);
					addEscola(etmp);
					break;
				default:
					break;
			}
		}
	}
}

void Grafo::addProfessor(Professor professor){
	this->professores.push_back(professor);
}

void Grafo::addEscola(Escola escola){
	this->escolas.push_back(escola);
}

Professor Grafo::splitProfessor(string professor){
	Professor ptmp;
	string buf;
	unsigned int indicei, indicef, i;
	indicei=professor.find_first_of('P');
	indicef=professor.find_first_of(',');
	buf=professor.substr(indicei+1,indicef-indicei-1);
	ptmp.setId(stoi(buf));
	indicei=indicef+1;
	indicef=professor.find_first_of(')');
	buf=professor.substr(indicei,indicef-indicei);
	ptmp.setHabilitacoes(stoi(buf));
	indicei=indicef+3;
	for(i=indicei;professor[i]!=')';i++){
		if(professor[i]=='E'){
			indicei=i;
		}
		else if(professor[i]==','){
			indicef=i;
			buf=professor.substr(indicei+1,indicef-indicei-1);
			ptmp.addPreferencia(stoi(buf));
		}
	}
	buf=professor.substr(indicei+1);
	buf.pop_back();
	ptmp.addPreferencia(stoi(buf));
	cout<<ptmp;
	return ptmp;
}

Escola Grafo::splitEscola(string escola){
	unsigned int indicei, indicef, i;
	Escola etmp;
	string buf;
	indicei=escola.find_first_of('E');
	indicef=escola.find_first_of(')');
	buf=escola.substr(indicei+1,indicef-indicei-1);
	etmp.setId(stoi(buf));
	indicei=indicef+3;
	for (i=indicei;escola[i]!=')';i++);
	buf=escola.substr(indicei,i-indicei);
	etmp.setPreferencia(stoi(buf));
	indicei=escola.find_last_of('(');
	buf=escola.substr(indicei+1);
	buf.pop_back();
	etmp.setVagas(stoi(buf));
	cout<<etmp;
	return etmp;
}