#ifndef GRAFO_H
#define GRAFO_H

#include "professor.h"
#include "escola.h"
#include "vector"
#include "iostream"
#include "string"
#include "fstream"

using namespace std;

class Grafo{
public:
	Grafo();
	vector<Professor> getProfessores();
	vector<Escola> getEscolas();
	void addItem(string);
	void fillGrafo();
	void fillGrafo(string);
private:
	vector<Professor> professores;
	vector<Escola> escolas;
	void addProfessor(Professor);
	void addEscola(Escola);
	Professor splitProfessor(string);
	Escola splitEscola(string);
};

#endif