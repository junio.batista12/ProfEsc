#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "vector"
#include "iostream"

using namespace std;

class Professor{
private:
	int id, habilitacoes;
	vector<int> preferencias;
public:
	Professor();
	int getId();
	int getHabilitacoes();
	vector<int> getPreferencias();
	void setId(int);
	void setHabilitacoes(int);
	void addPreferencia(int);
	friend ostream& operator<<(ostream&, const Professor&);
};

#endif